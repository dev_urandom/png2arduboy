# png2arduboy
---

**png2arduboy** is a simple and primitive converter from PNG images to Arduboy
data that can be used with the `drawBitmap()` function.

Unlike regular 1-bit-per-pixel bitmaps, Arduboy's bitmap data data is arranged
by storing a 1x8 vertical column of pixels for every byte, as follows:

```
aa ii ...
bb jj ...
cc kk ...
dd ll ...
ee mm ...
ff nn ...
gg oo ...
hh pp ...
```

## Compilation

The program depends only on `libpng` and compiles into a single executable file
after running `make`.

## Usage

`$ png2ard [args] filename`

The possible `[args]` are:

* `-h`: instead creates a more traditional tileset, where pixels in each tile
  are laid horizontally, not vertically. Not for use with Arduboy.

* `-i`: invert the image, so that black pixels correspond to `1` and white
  pixels correspond to 0.

* `-n #`: instead of printing a new line every time a row of pixels is
  converted, print one every # pixels.

The program will output the bitmap data in C array format, similar to the format
used by the `xxd` tool when called with the `-i` flag, but without the actual
array definition.

The program will try to accept any PNGs it can, whether they are monochrome,
paletted, colored, with or without transparency (it asks `libpng` to internally
transform them into 8-bit grayscale images), but some glitches can occur.
