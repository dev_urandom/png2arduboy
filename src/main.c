#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>
#include <png.h>
#include <unistd.h>

bool horiz = false;
bool invert = false;
int newline = 0;

int readtiles(const char* filename) {
	FILE* infile = fopen(filename,"rb");
	if (!infile) perror ("Unable to open file");

	unsigned char pngsig[8];
	fread(pngsig,1,8,infile);
	if (!png_check_sig(pngsig,8))
		return 1; //bad PNG signature

	png_structp png_ptr;
	png_infop info_ptr;

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) return 4;

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		png_destroy_read_struct(&png_ptr,NULL,NULL);
		return 4;
	}

	//if (setjmp(png_ptr->jmpbuf)) {
	//	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	//	return 2;
	//}

	png_init_io(png_ptr, infile);
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);

	png_uint_32 width=0, height=0;
	int bit_depth=0, color_type=0;

	png_get_IHDR(png_ptr,info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);

	//printf("Loaded %dx%d %dbpp %d\n",width,height,bit_depth,color_type);

	if (horiz && (width%8)) {
		fprintf(stderr,"Error: image width is not a multiple of 8.\n");	
	}

	if (height%8) {
		fprintf(stderr,"Error: image height is not a multiple of 8.\n");	
	}

	if (color_type & PNG_COLOR_TYPE_PALETTE) {
		png_set_palette_to_rgb(png_ptr);
		png_set_rgb_to_gray(png_ptr,1,-1.0,-1.0);
		png_set_strip_alpha(png_ptr);
	}

	if (color_type & PNG_COLOR_TYPE_RGB) {
		png_set_rgb_to_gray(png_ptr,1,-1.0,-1.0);
	}

	if (bit_depth < 8) {
		png_set_expand_gray_1_2_4_to_8(png_ptr);
	}

	png_read_update_info(png_ptr,info_ptr);
	png_get_IHDR(png_ptr,info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);

	//printf("Converted to %dx%d %dbpp %d\n",width,height,bit_depth,color_type);

	if (bit_depth != 8) {
		fprintf(stderr,"Somehow, after all these conversions, the end result isn't 8bpp.\n");
		exit(1);
	}

	png_bytep bytes = malloc(width * height);

	png_bytep rows[height];
	for (int i=0; i < height; i++) rows[i] = &(bytes[i*width]);

	png_read_image(png_ptr, rows);

	if (invert) {
		for (int i=0; i < (width*height); i++) bytes[i] ^= 0xFF; //invert all bytes
	}

	uint8_t sprite[(height * width / 8)];
	memset(sprite,0,(height * width / 8));

	if (horiz) {
		
		for (int iy=0; iy < height; iy++) {
			for (int ix=0; ix < width; ix++) {
				sprite[ ((iy / 8) * width) + ((ix / 8) * 8) + (iy % 8) ] |= ( (bytes[iy*width+ix] >= 128) << (7 - (ix%8)) );
			}
		}

	} else {
		
		for (int iy=0; iy < height; iy++) {
			for (int ix=0; ix < width; ix++) {
				sprite[ (iy/8) * width + ix ] |= ( (bytes[iy*width+ix] >= 128) << (iy%8) );
			}
		}

	}

	printf("{\n");

	for (int iy=0; iy < (height/8); iy++) {
		for (int ix=0; ix < width; ix++) {
			printf("0x%02x, ",sprite[iy*width+ix]);
			if (newline && ((ix % newline) == (newline-1)) ) printf("\n");
		}
		if (!newline) printf("\n");
	}
	printf("};\n");
	
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	fclose(infile);

	return 0;
}

int main(int argc, char** argv) {

	int opt;

	while ((opt = getopt(argc,argv,"hin:")) != -1) {
		switch(opt) {
		case 'h':
			horiz = true;
			break;
		case 'i':
			invert = true;
			break;
		case 'n':
			newline = atoi(optarg);
			break;
		}
	}

	if (optind >= argc) {
		printf(	"Usage: %s [args] [filename]\n"
			"args are:\n"
			"  -h -- store pixels in each tile in horizontal order instead of vertical\n"
			"  -i -- invert the outputted sprite\n"
			"  -n [x] -- add a newline every n bytes\n"
			,argv[0]);
		
		return 1;
	}
	return readtiles(argv[optind]);
}
